from django import forms
from .models import DonateModel
from .models import Feedback

class DonateForm(forms.ModelForm):
    Jumlah = forms.IntegerField(min_value=10000, initial=10000)
    class Meta:
        model = DonateModel
        fields = ['Nama', 'Rekening', 'Bank', 'Jumlah', 'Pesan']
        widgets = {
            'Nama': forms.TextInput(attrs={'placeholder': 'Tidak harus nama asli'}),
            'Rekening': forms.TextInput(attrs={'placeholder': 'Nama pemilik rekening'}),
            'Jumlah': forms.NumberInput(attrs={'placeholder': 'Minimum Donasi Rp10.0000'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = ['Feedback']
        widget = {
            'Feedback': forms.TextInput(attrs={'placeholder': 'Tulis feedback mu disini'})
        }