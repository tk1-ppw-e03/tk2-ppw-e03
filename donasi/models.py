from django.db import models
from django.core.validators import MinValueValidator 
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

# Create your models here.
class DonateModel(models.Model):
    Nama = models.CharField('Nama', max_length = 20)
    Rekening = models.CharField('Nama Rekening', max_length = 20)
    Bank = models.CharField('Nama Bank', max_length = 20)
    Jumlah = models.PositiveIntegerField(default=10000, validators=[MinValueValidator(10000)])
    Pesan = models.TextField()
    username = models.ForeignKey(User, null=True, on_delete=models.CASCADE)

class Feedback(models.Model):
    Feedback = models.CharField(max_length=500)