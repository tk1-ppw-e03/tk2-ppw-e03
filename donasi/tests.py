from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest, response
from .models import DonateModel, Feedback
from .views import create, payment, list_donate, feedback
from http import HTTPStatus
from django.contrib.auth.models import User


# Create your tests here.
class DonateTestEverything(TestCase):
    def test_url_donate_payment_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        donate = DonateModel.objects.create(
            Nama = 'Astrid',
            Rekening = 'Astrid',
            Bank = 'BinkBunk',
            Jumlah = '10000',
            Pesan = 'ah mantap',
            username = user
        )
        url = "/donate/payment/" + str(user.username)+"."+str(donate.id)+"/"
        response = c.get(url)
        self.assertEquals(response.status_code, 200)

    def test_url_donate_payment_without_login(self):
        response = Client().get('/donate/payment/testing.1/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/login-pepew?next=/donate/payment/testing.1/')
    
    def test_url_donate_feedback_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        donate = Feedback.objects.create(
            Feedback = 'Keren'
        )
        url = "/donate/feedback/"
        response = c.get(url)
        self.assertEquals(response.status_code, 200)

    def test_url_donate_feedback_without_login(self):
        response = Client().get('/donate/feedback/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/login-pepew?next=/donate/feedback/')

    def test_url_donate_list_no_user_authenticated(self):
        response = Client().get('/donate/list/')
        self.assertEquals(response.status_code, 200)

    def test_template_formDonate_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/donate/')
        self.assertTemplateUsed(response, 'formDonate.html')

    def test_template_payment_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        donate = DonateModel.objects.create(
            Nama = 'Astrid',
            Rekening = 'Astrid',
            Bank = 'BinkBunk',
            Jumlah = '10000',
            Pesan = 'ah mantap',
            username = user
        )
        donate.save()
        response = c.get("/donate/payment/" +str(user.username)+"."+str(donate.id)+"/")
        self.assertTemplateUsed(response, 'payment.html')

    def test_template_listDonasi(self):
        response = Client().get('/donate/list/')
        self.assertTemplateUsed(response, 'listDonasi.html')

    def test_template_feedback_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        donate = Feedback.objects.create(
            Feedback = 'Keren'
        )
        donate.save()
        response = c.get("/donate/feedback/")
        self.assertTemplateUsed(response, 'reviewDonate.html')
        
    def test_donate_using_create_func(self):
        found = resolve('/donate/')
        self.assertEqual(found.func, create)

    def test_donatepayment_using_payment_func(self):
        found = resolve('/donate/payment/ast.1/')
        self.assertEqual(found.func, payment)

    def test_listDonate_using_list_donate_func(self):
        found = resolve('/donate/list/')
        self.assertEqual(found.func, list_donate)

    def test_feedback_using_feedback_func(self):
        found = resolve('/donate/feedback/')
        self.assertEqual(found.func, feedback)

    def test_model_DonateModel(self):
        DonateModel.objects.create(
            Nama = 'Astrid',
            Rekening = 'Astrid',
            Bank = 'BinkBunk',
            Jumlah = '10000',
            Pesan = 'ah mantap'
        )
        count = DonateModel.objects.all().count()
        self.assertEqual(count,1)

    def test_model_Feedback(self):
        Feedback.objects.create(
            Feedback = 'Keren'
        )
        count = Feedback.objects.all().count()
        self.assertEqual(count,1)
    
    def test_views_create(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.post("/donate/", data={
            "Nama":"Astrid",
            "Rekening":"Astrid",
            "Bank":"BinkBunk",
            "Jumlah":"10000",
            "Pesan":"ah mantap"
        })
        count = DonateModel.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(response.status_code,302)

    def test_views_feedback(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.post("/donate/feedback/", data={
            "Feedback":"Keren"
        })
        count = Feedback.objects.all().count()
        self.assertEqual(count,1)
        self.assertEqual(response.status_code,200)

    def test_donate_url_without_login(self):
        response = Client().get('/donate/')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/login-pepew?next=/donate/')
    
    def test_url_donate_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/donate/')
        self.assertEquals(response.status_code, 200)