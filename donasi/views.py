from django.shortcuts import redirect, render
from .models import DonateModel, Feedback
from . import forms
from django.http import HttpResponse
import random
import locale
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.forms.models import model_to_dict
# Create your views here.

def list_donate(request):
    donate=DonateModel.objects.all()
    return render(request, 'listDonasi.html',{'donate':donate})

@login_required(login_url='news:login')
def create(request):
    if request.method == 'POST':
        form = forms.DonateForm(request.POST)
        if form.is_valid():
            var = form.save()
            var.username = request.user
            var.save()
            return redirect('donasi:payment',var.username,var.id)
    else:
        form = forms.DonateForm()
        return render(request, 'formDonate.html', {'form': form})

@login_required(login_url='news:login')
def payment(request,username,payment_id):
    pay = DonateModel.objects.get(username=request.user, id=payment_id)
    rdm = random.randint(1,99)
    total = pay.Jumlah+rdm
    rupiah = '{:20,}'.format(total)
    payy = '{:20,}'.format(DonateModel.objects.get(username=request.user, id=payment_id).Jumlah)
    return render(request, 'payment.html', {'payy': payy, 'rdm':rdm, 'rupiah':rupiah})

@login_required(login_url='news:login')
def feedback(request):
     if request.method == 'POST':
        rev = forms.FeedbackForm(request.POST)
        if rev.is_valid():
            var = rev.save()
            return JsonResponse({'donate':model_to_dict(var)}, status=200)
        else:
            return redirect('donasi:feedback')

     else:
        rev = forms.FeedbackForm()
        res = Feedback.objects.all()
        return render(request, 'reviewDonate.html',context={'rev': rev, 'res':res})