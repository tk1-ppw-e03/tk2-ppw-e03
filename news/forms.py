from django.forms import ModelForm, TextInput, Textarea
from .models import News
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class CreateNews(ModelForm):
    class Meta:
        model = News
        fields = ['title', 'source', 'image_url', 'content']
        widgets = {
            'title': TextInput(attrs={'placeholder': 'Lebih dari 5.000 Kasus Covid-19 dalam 2 Hari, Epidemiolog: Ini Belum yang Terburuk'}),
            'source': TextInput(attrs={'placeholder': 'https://www.kompas.com/'}),
            'image_url': TextInput(attrs={'placeholder': 'https://unsplash.com/'}),
            'content': Textarea(attrs={'placeholder': 'Tuliskan isi beritamu disini'})
        }

class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']