$(document).ready(() => {
    const theParent = $('#id_image_url').parent().get(0);
    const image = `<div id="image-preview" class="image-section mt-2 mb-1"></div>`;
    $(theParent).append(image);
    checkImageURL();
})

$('#id_image_url').keyup(() => {
    checkImageURL();
})

const checkImageURL = () => {
    const value = $('#id_image_url').val();
    if (value != '') {
        $.ajax({
            url: value,
            success: function(data) {
                const image_success = `<img class="news-img" src="${value}" alt="News's image"></img>`
                $('#image-preview').empty();
                $('#image-preview').append(image_success);
                $('#save-button').attr('disabled', false);
            },
            error: function(data) {
                const image_error = `<p>Cannot found the image url</p>`
                $('#image-preview').empty();
                $('#image-preview').append(image_error);
                $('#save-button').attr('disabled', true);
            }
        });
    } else {
        $('#image-preview').empty();
        $('#save-button').attr('disabled', true);
    }
}

// Form-ajax
$('#form-news').submit(function(event) {
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/submission-news',
        dataType: 'json',
        data: $('form#form-news').serialize(),
        success: function(response) {
            document.getElementById('form-news').reset();
            checkImageURL();
            Swal.fire({
                icon: 'success',
                title: "It's a Success!",
                text: `Do you want to see "${response.title}"?`,
                showDenyButton: true,
                confirmButtonText: "Yes, of course",
                denyButtonText: "No, thanks"
            }).then((result) => {
                if (result.isConfirmed) {
                    $(location).attr('href', `/detail-news/${response.pk}`)
                }
            })
        },
        error: function(response) {
            if ($("input").next('p.help-block').length) {
                $("input").nextAll('p.help-block').empty();
            }
            error_message = response.responseJSON.error;
            const keys = Object.keys(error_message);
            for(let i = 0; i < keys.length; i++) {
                console.log(keys[i]);
                const inputElement = $(`#id_${keys[i]}`);
                error_message[keys[i]].forEach(item => {
                    inputElement.after(`<p class="help-block" style="color: #ff392e; font-weight:bold;">${item}</p>`)
                })
            };
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: `Please fix the error and submit it again 😢`,
                showConfirmButton: true,
                confirmButtonText: 'Okayy',
                timer: 3000,
                timerProgressBar: true,
            })
        }
    })
});