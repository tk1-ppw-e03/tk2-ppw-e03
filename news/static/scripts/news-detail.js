$( document ).ready(() => {
    const url = $(location).attr('pathname');
    const id = url.substring(13)
    $.ajax({
        method: 'GET',
        url: `/detail/news/${id}`,
        success: function(response) {
            const title = response.title;
            const date = response.date;
            const image_url = response.image_url;
            const content = response.content;
            const source = response.source;
            const data = `
                <div class="image-section">
                    <img class="news-img" src="${image_url}" alt="News's image">
                </div>
                <h2 class="news-title">${title}</h2>
                <p class="news-date">${date}</p>
                <div class="blue-line"></div>
                <p class="news-content">${content}</p>
                <p class="news-source">Sumber : ${source}</p>
                <div class="button-group">
                    <a class="btn pepew-button" href="/list-news"><i class="fas fa-arrow-left"></i>Kembali</a>
                </div>
            `;
            $('.container:first').append(data);
        },
        error: function(response) {
            const data = `
                <h2 class="sub-title">${response.responseJSON.error}</h2>
                <img src="/static/images/404.png" width="100%">
                <div class="button-group">
                    <a class="btn pepew-button" href="/list-news"><i class="fas fa-arrow-left"></i>Kembali</a>
                </div>
            `;
            $('.container:first').append(data);
        }
    });
});