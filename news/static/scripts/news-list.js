$( document ).ready(() => {
    $.ajax({
        method: 'GET',
        url: '/data-news',
        success: function(result) {
            $('#news-grid').empty();
            let data = '';
            if (result.length === 0) {
                data += '<h3>Belum ada berita😟</h3>';
            } else {
                result.forEach(news => {
                    const id = news.pk;
                    const title = news.fields.title;
                    const date = news.fields.date;
                    const image_url = news.fields.image_url;
                    data += `
                        <a href="/detail-news/${id}">
                            <div class="card">
                                <img class="card-img-top" src="${image_url}" alt="News's image">
                                <p class="date">${date}</p>
                                <h3 class="card-title">${title}</h3>
                            </div>
                        </a>
                    `
                });
            }
            $('#news-grid').append(data);
        }
    });
});

$('#tulis').hover(function(event) {
    $(this).empty();
    $(this).append('<i class="fas fa-pen" style="line-height: 36px;"></i>')
}, function(event) {
    $(this).empty();
    $(this).append('Tulis');
})