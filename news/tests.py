from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .models import News
from .views import news_list, news_detail, news_create, loginPage, registerPage, logoutUser
from django.contrib.auth.models import User

# Create your tests here.
class NewsTest(TestCase):
    # Test URL
    def test_list_news_url_is_exist(self):
        response = Client().get('/list-news')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_list_news_using_news_list_func(self):
        found = resolve('/list-news')
        self.assertEqual(found.func, news_list)

    def test_detail_news_url_is_exist(self):
        new_news = News.objects.create(
            title = 'Judul Berita',
            source = 'Sumber Berita',
            image_url = 'Gambar Berita',
            content = 'Konten Berita'
        )
        my_url = "/detail-news/" + str(new_news.id)
        response = Client().get(my_url)
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_detail_news_url_wrong_id(self):
        response = Client().get('/detail-news/1')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_detail_news_using_news_detail_func(self):
        found = resolve('/detail-news/1')
        self.assertEqual(found.func, news_detail)

    def test_create_news_using_news_create_func(self):
        found = resolve('/create-news')
        self.assertEqual(found.func, news_create)

    def test_create_news_url_without_login(self):
        response = Client().get('/create-news')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/login-pepew?next=/create-news')
    
    def test_create_news_url_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/create-news')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    # Test Model
    def test_create_news(self):
        News.objects.create(
            title = 'Judul Berita',
            source = 'Sumber Berita',
            image_url = 'Gambar Berita',
            content = 'Konten Berita'
        )
        counter = News.objects.all().count()
        self.assertEqual(counter, 1)

    # Test Views
    def test_post_news_form(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.post(
            "/submission-news",
            data = {
                "title" : 'Judul Berita',
                "source" : 'Sumber Berita',
                "image_url" : 'Gambar Berita',
                "content" : 'Konten Berita'
            }
        )
        self.assertEqual(response.status_code, HTTPStatus.OK)
        counter = News.objects.all().count()
        self.assertEqual(counter, 1)

    def test_post_news_form_failed(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.post(
            "/submission-news",
            data = {
                "source" : 'Sumber Berita',
                "image_url" : 'Gambar Berita',
                "content" : 'Konten Berita'
            }
        )
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)
        counter = News.objects.all().count()
        self.assertEqual(counter, 0)

    def test_get_news_submission(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get("/submission-news")
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)
    
    def test_news_data_ajax(self):
        response = Client().get('/data-news')
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_detail_news_ajax_success(self):
        new_news = News.objects.create(
            title = 'Judul Berita',
            source = 'Sumber Berita',
            image_url = 'Gambar Berita',
            content = 'Konten Berita'
        )
        my_url = "/detail/news/" + str(new_news.id)
        response = Client().get(my_url)
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_detail_news_ajax_failed(self):
        response = Client().get('/detail/news/1')
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {"error": "Berita tidak ditemukan!"}
        )

class UserAccountTest(TestCase):
    # Test function
    def test_login_url_using_loginpage_func(self):
        found = resolve('/login-pepew')
        self.assertEqual(found.func, loginPage)

    def test_register_url_using_registerpage_func(self):
        found = resolve('/register-pepew')
        self.assertEqual(found.func, registerPage)

    def test_logout_url_using_logoutuser_func(self):
        found = resolve('/logout-pepew')
        self.assertEqual(found.func, logoutUser)

    # Test URL
    def test_list_news_url_no_user_authenticated(self):
        response = Client().get('/list-news')
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertIn("Login", html_contents)
        self.assertNotIn("Ingin menambahkan berita baru ?", html_contents)
        self.assertNotIn("Tulis", html_contents)
        self.assertNotIn("Logout", html_contents)

    def test_list_news_url_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/list-news')
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertNotIn("Login", html_contents)
        self.assertIn("Ingin menambahkan berita baru ?", html_contents)
        self.assertIn("Tulis", html_contents)
        self.assertIn("Logout", html_contents)

    def test_register_url_wrong_data(self):
        response = Client().post('/register-pepew', data={
            "username": "testing",
            "password1": "maululus1112",
            "password2": "maululus"
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        counter = User.objects.all().count()
        self.assertEqual(counter, 0)

    def test_register_url_correct_data(self):
        response = Client().post('/register-pepew', data={
            "username": "testing",
            "password1": "maululus1112",
            "password2": "maululus1112"
        })
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/login-pepew')
        counter = User.objects.all().count()
        self.assertEqual(counter, 1)

    def test_register_url_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/register-pepew')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/')

    def test_login_url_wrong_data(self):
        response = Client().post('/login-pepew', data={
            "username": "testing",
            "password": "maululus",
        })
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertIn('Username or password is incorrect', html_contents)

    def test_login_url_correct_data(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        response = Client().post('/login-pepew', data={
            "username": "testing",
            "password": "maululus1112",
        })
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/')

    def test_login_url_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/login-pepew')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/')

    def test_logout_url(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/logout-pepew')
        self.assertEquals(response.status_code, HTTPStatus.FOUND)
        self.assertRedirects(response, '/')
