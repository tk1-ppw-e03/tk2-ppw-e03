from django.urls import path
from . import views

app_name = 'news'
urlpatterns = [
    path('list-news', views.news_list, name="news_list"),
    path('detail-news/<int:id>', views.news_detail, name="news_detail"),
    path('create-news', views.news_create, name="news_create"),
    path('data-news', views.news_data, name="news_data"),
    path('submission-news', views.news_submission, name="news_submission"),
    path('detail/news/<int:id>', views.news_get_detail, name="news_get_detail"),
    path('login-pepew', views.loginPage, name="login"),
    path('register-pepew', views.registerPage, name="register"),
    path('logout-pepew', views.logoutUser, name="logout"),
]