from django.shortcuts import render, redirect
from .models import News
from .forms import CreateNews, RegisterForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.http import HttpResponse, JsonResponse

# Create your views here.
def news_data(request):
    news = News.objects.all().order_by('date')
    list_news = serializers.serialize('json', news)
    return HttpResponse(list_news, content_type="application/json")

@login_required(login_url='news:login')
def news_submission(request):
    if request.method == 'POST':
        form = CreateNews(request.POST)
        if form.is_valid():
            item = form.save()
            response_data = {
                'pk': item.pk,
                'title': item.title,
            }
            response = JsonResponse(response_data)
            response.status_code = 200
        else :
            response_data = {'error': form.errors}
            response = JsonResponse(response_data)
            response.status_code = 404
        return response
    response_data = {'error': 'Please try again later!'}
    response = JsonResponse(response_data)
    response.status_code = 404
    return response

def news_get_detail(request, id):
    item = News.objects.filter(pk = id)
    if item.exists():
        item =item[0]
        response_data = {
            'pk': item.pk,
            'title': item.title,
            'date': item.date,
            'source': item.source,
            'image_url': item.image_url,
            'content': item.content
        }
        response = JsonResponse(response_data)
        response.status_code = 200
        return response
    response_data = {'error': 'Berita tidak ditemukan!'}
    response = JsonResponse(response_data)
    response.status_code = 404
    return response

def news_list(request):
    return render(request, "pages/news-list.html")

def news_detail(request, id):
	#return detail of news
    return render(request, "pages/news-detail.html")

@login_required(login_url='news:login')
def news_create(request):
    form = CreateNews()
    return render(request, "pages/news-create.html", {'form' : form})

# Views for login
def loginPage(request):
    if request.user.is_authenticated:
        return redirect('pepew_home:home_url')
    else:
        html = 'user/login.html'
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username = username, password = password)

            if user is not None:
                login(request, user)
                return redirect(request.POST.get('next') or 'pepew_home:home_url')
            else:
                messages.info(request, 'Username or password is incorrect')
                return render(request, html)
        return render(request, html)

def registerPage(request):
    if request.user.is_authenticated:
        return redirect('pepew_home:home_url')
    else:
        form = RegisterForm(request.POST or None)

        if request.method == 'POST':
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user)
                return redirect('news:login')

        context = {'form' : form}
        html = 'user/register.html'
        return render(request, html, context)

def logoutUser(request):
    logout(request)
    return redirect('pepew_home:home_url')