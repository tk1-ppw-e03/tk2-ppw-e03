from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .views import home_view
from django.contrib.auth.models import User

# Create your tests here.
class HomeTest(TestCase):
    # Test Home
    def test_home_url_is_exist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, HTTPStatus.OK)

    def test_home_url_using_home_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_view)

    def test_home_url_no_user_authenticated(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertIn("Login", html_contents)
        self.assertIn("Sign Up", html_contents)
        self.assertNotIn("Logout", html_contents)

    def test_home_url_with_user_logged_in(self):
        user = User.objects.create_user(username="testing", password="maululus1112")
        user.save()
        c = Client()
        c.login(username="testing", password="maululus1112")
        response = c.get('/')
        self.assertEquals(response.status_code, HTTPStatus.OK)
        html_contents = response.content.decode('utf8')
        self.assertNotIn("Login", html_contents)
        self.assertNotIn("Sign Up", html_contents)
        self.assertIn("Logout", html_contents)