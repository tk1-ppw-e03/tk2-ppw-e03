from django.urls import path
from . import views


app_name = 'pepew_home'
urlpatterns = [
    path('', views.home_view, name="home_url"),
]