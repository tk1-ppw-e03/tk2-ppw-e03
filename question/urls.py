from django.urls import path
from . import views

app_name = 'question'
urlpatterns = [
    path('question', views.create, name='create'),
    path('list/', views.list_question, name='list_question'),
]