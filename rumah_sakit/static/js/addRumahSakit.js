$(document).ready(function(){
    $(".message").hide()

    $("#rs-form").submit(function(e) {
        e.preventDefault();
        $(".message").empty()
        $.ajax({
            url: 'create-rs/',
            data: $(this).serialize(),
            dataType: 'json',
            type: 'POST',
            success: function (hasil) {
                $("#rs-form").trigger('reset');
                $(".message").append(hasil);
                $(".message").show()
            }
        })
    });
});