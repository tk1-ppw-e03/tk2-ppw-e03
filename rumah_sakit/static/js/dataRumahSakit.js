$(document).ready(function(){
    var styleBlock = '<style id="placeholder-style"></style>';
    $('head').append(styleBlock);

    $(".search-box").hide();

    $("#hide-search-box").click(function(){
        $(".search-box").hide();
        $(".search-btn").show();
        $("#keyword").val('');
        search();
    });
    
    $("#show-search-box").click(function(){
        $(".search-btn").hide();
        $(".search-box").show();
    });

    $("#search-icon").click(function(){
        $("#keyword").focus();
    });

    $("#keyword").focusin(function(){
        $(".search-box").css({"background-color": "#E3F6F5", "color": "#272343"});
        $("#keyword ").css({"color": "#272343", "border-bottom": "1px solid #272343"});
        var styleContent = '#keyword:-moz-placeholder {color:#272343;} #keyword::-webkit-input-placeholder {color: #272343;}'
        $('#placeholder-style').text(styleContent);
    });

    $("#keyword").focusout(function(){
        $(".search-box").css({"background-color": "#272343", "color": "#FEFEFE"});
        $("#keyword ").css({"color": "#FEFEFE", "border-bottom": "1px solid #FEFEFE"});
        var styleContent = '#keyword:-moz-placeholder {color:#FEFEFE;} #keyword::-webkit-input-placeholder {color: #FEFEFE;}'
        $('#placeholder-style').text(styleContent);
    });

    $("#keyword").keyup(search);
    
    function search() {
        var keyword = $("#keyword").val();
        var url = 'search/?q=' + keyword;
        $.ajax({
            url: url,
            success: function(hasil) {
                var hasilKeyword = $('#rs');
                hasilKeyword.empty();

                if (hasil.length == 0) {
                    hasilKeyword.append('<tr><td class="text-center" colspan="3">' + 
                    'Rumah sakit dengan nama "' + keyword + '" tidak ditemukan</td></tr>');
                } else {
                    for (i = 0; i < hasil.length; i++) {
                        var tmp_id = hasil[i].pk;
                        var tmp_nama = hasil[i].fields["nama"];
                        var tmp_provinsi = hasil[i].fields["provinsi"];
                        hasilKeyword.append('<tr><td>' + (i+1) + '</td>' 
                        + '<td><a class="link-detail c-birutua" '
                        + 'href="detail-rumah-sakit/' + tmp_id + '/">' + tmp_nama + '</a></td>'
                        + '<td>' + tmp_provinsi + '</td></tr>');
                    }
                }
            }
        })
    }
});