from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User
import json

from .views import dataRumahSakit, addRumahSakit, detailRumahSakit, removeRumahSakit, search, createRS
from .models import RumahSakit

class RumahSakitTest(TestCase):

    # Test URL
    def test_dataRumahSakit_url_is_exist(self):
        response = Client().get('/data-rumah-sakit/')
        self.assertEqual(response.status_code, 200)

    def test_addRumahSakit_url_is_exist_if_a_user_logged_in(self):
        User.objects.create_user(username="test", password="test1234")
        c = Client()
        c.login(username="test", password="test1234")
        response = c.get('/data-rumah-sakit/add-rumah-sakit/')
        self.assertEqual(response.status_code, 200)
    
    def test_addRumahSakit_url_will_be_redirect_to_loginPage_if_no_user_authenticated(self):
        response = Client().get('/data-rumah-sakit/add-rumah-sakit/')
        self.assertRedirects(response, '/login-pepew?next=/data-rumah-sakit/add-rumah-sakit/')

    def test_detailRumahSakit_url_is_exist(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = Client().get('/data-rumah-sakit/detail-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertEqual(response.status_code, 200)

    def test_removeRumahSakit_url_will_be_redirect_to_dataRumahSakit_if_a_user_logged_in(self):
        User.objects.create_user(username="test", password="test1234")
        c = Client()
        c.login(username="test", password="test1234")
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = c.get('/data-rumah-sakit/remove-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertRedirects(response, '/data-rumah-sakit/')

    def test_removeRumahSakit_url_will_be_redirect_to_loginPage_if_no_user_authenticated(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = Client().get('/data-rumah-sakit/remove-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertRedirects(response, '/login-pepew?next=/data-rumah-sakit/remove-rumah-sakit/' + str(rumahSakit_baru.id)  + '/')

    #Test HTML Template
    def test_dataRumahSakit_using_dataRumahSakit_template(self):
        response = Client().get('/data-rumah-sakit/')
        self.assertTemplateUsed(response, 'rumah_sakit/dataRumahSakit.html')

    def test_addRumahSakit_using_addRumahSakit_template_with_a_user_logged_in(self):
        User.objects.create_user(username="test", password="test1234")
        c = Client()
        c.login(username="test", password="test1234")
        response = c.get('/data-rumah-sakit/add-rumah-sakit/')
        self.assertTemplateUsed(response, 'rumah_sakit/addRumahSakit.html')

    def test_detailRumahSakit_using_detailRumahSakit_template(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = Client().get('/data-rumah-sakit/detail-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertTemplateUsed(response, 'rumah_sakit/detailRumahSakit.html')

    def test_dataRumahSakit_template_is_complete_with_no_user_authenticated(self):
        response = Client().get('/data-rumah-sakit/')
        html_response = response.content.decode('utf8')
        self.assertIn("Login", html_response)
        self.assertNotIn("Logout", html_response)
        self.assertNotIn('Tambah Rumah Sakit', html_response)
        self.assertIn('No', html_response)
        self.assertIn('Nama Rumah Sakit', html_response)
        self.assertIn('Provinsi', html_response)

    def test_dataRumahSakit_template_is_complete_with_a_user_logged_in(self):
        User.objects.create_user(username="test", password="test1234")
        c = Client()
        c.login(username="test", password="test1234")
        response = c.get('/data-rumah-sakit/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("Login", html_response)
        self.assertIn("Logout", html_response)
        self.assertIn('Tambah Rumah Sakit', html_response)
        self.assertIn('No', html_response)
        self.assertIn('Nama Rumah Sakit', html_response)
        self.assertIn('Provinsi', html_response)

    def test_addRumahSakit_template_is_complete_with_a_user_logged_in(self):
        User.objects.create_user(username="test", password="test1234")
        c = Client()
        c.login(username="test", password="test1234")
        response = c.get('/data-rumah-sakit/add-rumah-sakit/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("Login", html_response)
        self.assertIn("Logout", html_response)
        self.assertIn('Kembali', html_response)
        self.assertIn('Tambah Data Rumah Sakit', html_response)
        self.assertIn('Nama Rumah Sakit', html_response)
        self.assertIn('Alamat', html_response)
        self.assertIn('Provinsi', html_response)
        self.assertIn('No Telepon', html_response)
        self.assertIn('Fax', html_response)
        self.assertIn('Tambah', html_response)

    def test_detailRumahSakit_template_is_complete_with_no_user_authenticated(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = Client().get('/data-rumah-sakit/detail-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        html_response = response.content.decode('utf8')
        self.assertIn("Login", html_response)
        self.assertNotIn("Logout", html_response)
        self.assertIn('Kembali', html_response)
        self.assertNotIn('Hapus', html_response)
        self.assertIn('Rumah Sakit A', html_response)
        self.assertIn('Jalan Melati Kav.70', html_response)
        self.assertIn('DKI Jakarta', html_response)
        self.assertIn('02142424242', html_response)
        self.assertIn('02188888888', html_response)

    def test_detailRumahSakit_template_is_complete_with_a_user_logged_in(self):
        User.objects.create_user(username="test", password="test1234")
        c = Client()
        c.login(username="test", password="test1234")
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        response = c.get('/data-rumah-sakit/detail-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        html_response = response.content.decode('utf8')
        self.assertNotIn("Login", html_response)
        self.assertIn("Logout", html_response)
        self.assertIn('Kembali', html_response)
        self.assertIn('Hapus', html_response)
        self.assertIn('Rumah Sakit A', html_response)
        self.assertIn('Jalan Melati Kav.70', html_response)
        self.assertIn('DKI Jakarta', html_response)
        self.assertIn('02142424242', html_response)
        self.assertIn('02188888888', html_response)
        
    #Test View
    def test_dataRumahSakit_using_dataRumahSakit_func(self):
        found = resolve('/data-rumah-sakit/')
        self.assertEqual(found.func, dataRumahSakit)

    def test_addRumahSakit_using_addRumahSakit_func(self):
        found = resolve('/data-rumah-sakit/add-rumah-sakit/')
        self.assertEqual(found.func, addRumahSakit)

    def test_detailRumahSakit_using_detailRumahSakit_func(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        found = resolve('/data-rumah-sakit/detail-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertEqual(found.func, detailRumahSakit)

    def test_removeRumahSakit_using_removeRumahSakit_func(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta', 
            telepon='02142424242', 
            fax='02188888888')
        found = resolve('/data-rumah-sakit/remove-rumah-sakit/' + str(rumahSakit_baru.id) + '/')
        self.assertEqual(found.func, removeRumahSakit)

    #Test Model
    def test_model_can_create_new_rumahSakit(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta',
            telepon='02142424242', 
            fax='02188888888')
        jumlah_rumahSakit = RumahSakit.objects.all().count()
        self.assertEqual(jumlah_rumahSakit, 1)
        self.assertEqual(str(rumahSakit_baru), 'Rumah Sakit A')

    #Test Ajax Call
    def test_search_using_search_func(self):
        found = resolve('/data-rumah-sakit/search/')
        self.assertEqual(found.func, search)

    def test_search_url_is_exits(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta',
            telepon='02142424242', 
            fax='02188888888')
        keyword = "A"
        response = Client().get('/data-rumah-sakit/search/?q=' + keyword)
        self.assertEqual(response.status_code, 200)

    def test_ajax_search_if_an_object_models_is_found_with_a_specific_keyword(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta',
            telepon='02142424242', 
            fax='02188888888')
        keyword = "A"
        response = Client().get('/data-rumah-sakit/search/?q=' + keyword)
        json_response = response.content
        response_data = json.loads(json_response)
        self.assertEqual(len(response_data), 1)

    def test_ajax_search_if_no_object_models_is_found_with_a_specific_keyword(self):
        rumahSakit_baru = RumahSakit.objects.create(
            nama='Rumah Sakit A', 
            alamat='Jalan Melati Kav.70', 
            provinsi='DKI Jakarta',
            telepon='02142424242', 
            fax='02188888888')
        keyword = "B"
        response = Client().get('/data-rumah-sakit/search/?q=' + keyword)
        json_response = response.content
        response_data = json.loads(json_response)
        self.assertEqual(len(response_data), 0)

    def test_create_rs_using_createRS_func(self):
        found = resolve('/data-rumah-sakit/add-rumah-sakit/create-rs/')
        self.assertEqual(found.func, createRS)

    def test_post_rumahSakit_form_with_ajax_call(self):
        User.objects.create_user(username="test", password="test1234")
        c = Client()
        c.login(username="test", password="test1234")
        data = {
            'nama': 'Rumah Sakit A', 
            'alamat': 'Jalan Melati Kav.70', 
            'provinsi': 'DKI Jakarta',
            'telepon': '02142424242', 
            'fax': '02188888888'
        }
        response = c.post("/data-rumah-sakit/add-rumah-sakit/create-rs/", data=data)
        self.assertEqual(response.status_code, 200)
        counter = RumahSakit.objects.all().count()
        self.assertEqual(counter, 1)
        json_response = response.content
        response_data = json.loads(json_response)
        self.assertEqual("Rumah sakit dengan nama \"" + data["nama"] + "\" berhasil ditambahkan", response_data[0])

    def test_post_rumahSakit_form_failed_with_ajax_call(self):
        User.objects.create_user(username="test", password="test1234")
        c = Client()
        c.login(username="test", password="test1234")
        data = {
            'nama': 'Rumah Sakit A', 
            'alamat': 'Jalan Melati Kav.70', 
            'telepon': '02142424242', 
        }
        response = c.post("/data-rumah-sakit/add-rumah-sakit/create-rs/", data=data)
        self.assertEqual(response.status_code, 200)
        counter = RumahSakit.objects.all().count()
        self.assertEqual(counter, 0)
        json_response = response.content
        response_data = json.loads(json_response)
        self.assertEqual("Input yang dimasukkan salah", response_data[0])
