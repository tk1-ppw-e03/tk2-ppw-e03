from django.shortcuts import render, HttpResponseRedirect
from .models import RumahSakit
from .forms import AddRSForm
from django.contrib.auth.decorators import login_required
from django.core.serializers import serialize
import json
from django.http import JsonResponse

# Create your views here.
def dataRumahSakit(request):
    konteks = {
            'daftarRumahSakit': RumahSakit.objects.all()
        }
    return render(request, 'rumah_sakit/dataRumahSakit.html', konteks)

@login_required(login_url='news:login')
def addRumahSakit(request):
    konteks = {
            'form': AddRSForm()
        }
    return render(request, 'rumah_sakit/addRumahSakit.html', konteks)

def detailRumahSakit(request, pk):
    konteks = {
        'rumahSakit' : RumahSakit.objects.get(id=pk)
    }
    return render(request, 'rumah_sakit/detailRumahSakit.html', konteks)

@login_required(login_url='news:login')
def removeRumahSakit(request, pk):
    RumahSakit.objects.get(id=pk).delete()
    return HttpResponseRedirect('/data-rumah-sakit/')

def search(request):
    arg = request.GET['q']
    hasil = RumahSakit.objects.filter(nama__icontains=arg)
    data = serialize('json', hasil)
    return JsonResponse(json.loads(data), safe=False)

@login_required(login_url='news:login')
def createRS(request):
    message = []
    if request.method == 'POST':
        response = request.POST
        form = AddRSForm(response)
        if form.is_valid():
            form.save()
            message.append("Rumah sakit dengan nama \"" + response["nama"] + "\" berhasil ditambahkan")
        else:
            message.append("Input yang dimasukkan salah")
    return JsonResponse(message, safe=False)
