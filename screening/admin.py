from django.contrib import admin
from .models import Answer

# Register your models here.

@admin.register(Answer)
class ScreeningAdmin(admin.ModelAdmin):
    pass