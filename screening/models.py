from django.db import models
from django.core.exceptions import ValidationError

# Create your models here.


class Question(models.Model):
    question = models.TextField()

    def __str__(self):
        return self.question


class Answer(models.Model):
    is_Fever = models.BooleanField(default=False)
    is_HardBreathing = models.BooleanField(default=False)
    is_Cough = models.BooleanField(default=False)
    is_Vommitting = models.BooleanField(default=False)
    is_Pain = models.BooleanField(default=False)
    is_No = models.BooleanField(default=False)

    Symptom = [
        is_Fever,
        is_HardBreathing,
        is_Cough,
        is_Vommitting,
        is_Pain,
        is_No,
    ]

    SympTrue = []

    is_Obesity = models.BooleanField(default=False)
    is_SmokeVape = models.BooleanField(default=False)
    is_Pregnant = models.BooleanField(default=False)
    is_CirculatoryDisease = models.BooleanField(default=False)
    is_BloodCellDisorder = models.BooleanField(default=False)
    is_CondNull = models.BooleanField(default=False)

    Condition = [
        is_Obesity,
        is_SmokeVape,
        is_Pregnant,
        is_CirculatoryDisease,
        is_BloodCellDisorder,
        is_CondNull,
    ]

    CondTrue = []

    age = models.CharField(max_length=100)
    test = models.CharField(max_length=100)
    work = models.CharField(max_length=100)
