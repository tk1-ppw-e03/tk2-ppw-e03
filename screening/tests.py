from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import resolve
from .views import landing, tnc, question, result
from django.contrib.auth.models import User
from .models import Answer


# Create your tests here.
class ScreeningTest(TestCase):
    def test_screening_url_is_exist(self):
        response = Client().get('/Screening/')
        self.assertEqual(response.status_code, 200)

    def test_screening_url_is_using_landing(self):
        found = resolve('/Screening/')
        self.assertEqual(found.func, landing)

    def test_tnc_screening_url_is_exist(self):
        response = Client().get('/Screening/Privacy/')
        self.assertEqual(response.status_code, 200)

    def test_tnc_screening_url_is_using_tnc(self):
        found = resolve('/Screening/Privacy/')
        self.assertEqual(found.func, tnc)

    def test_if_questionnaire_url_is_exist(self):
        response = Client().get('/Screening/Questionnaire/')
        self.assertEqual(response.status_code, 200)

    def test_if_questionnaire_url_is_using_question(self):
        found = resolve('/Screening/Questionnaire/')
        self.assertEqual(found.func, question)

    def test_if_user_is_logged_in(self):
        user = User.objects.create_user(
            username="aryaseputra", email="aryaseputra1@gmail.com", password="teka-pepewe")
        user.save()
        self.client.login(username="aryaseputra", password="teka-pepewe")
        response = self.client.get("/Screening/Privacy/")
        self.assertContains(response, "Mulai")

    def test_if_user_is_not_logged_in(self):
        response = self.client.get("/Screening/Privacy/")
        self.assertContains(response, "Anda belum login")

    def test_add_object(self):
        context = Answer(
            age="18 - 64 tahun", test="Ya, Negatif Covid-19", work="Tidak bekerja/relawan di faskes")
        context.save()
        self.assertEqual(Answer.objects.all().count(), 1)

    def test_result(self):
        context = Answer(
            age="18 - 64 tahun", test="Ya, Negatif Covid-19", work="Tidak bekerja/relawan di faskes")
        context.save()
        response = self.client.get("/Screening/Result/")
        self.assertContains(response, context.age)

    def test_question(self):
        response = self.client.post(
            "/Screening/Questionnaire/", data={'age': "18 - 64 tahun", 'test': "Ya, Negatif Covid-19", 'work': "Tidak bekerja/relawan di faskes", 'symptomDemam': "True", 'symptomSulitBernapas': "True", 'symptomBatuk': "True", 'symptomMuntah': "True", 'symptomNyeri': "True", 'symptomNull': "True", 'Obesity': "True", 'SmokeVape': "True", 'Pregnant': "True", 'CirculatoryDisease': "True", 'BloodCellDisorder': "True", 'CondNull': "True"})
        self.assertEqual(Answer.objects.all().count(), 1)

        self.assertEqual(response.status_code, 302)
#        self.assertEqual(response['symptomDemam'], '/Screening/Questionnaire/')

        new_response = self.client.get("/Screening/Result/")
        html_response = new_response.content.decode('utf8')
        self.assertIn(
            'Gejala: terdapat 6 gejala, konsultasikan ke dokter segera.', html_response)
        self.assertIn(
            'Kondisi: terdapat 6 kondisi yang memenuhi.', html_response)
