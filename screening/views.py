from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from .models import Answer

# Create your views here.


def landing(request):
    return render(request, 'landing.html')


def tnc(request):
    return render(request, 'tnc.html')


def question(request):
    if request.method == 'POST':
        obj = Answer(age=request.POST.get("age"), test=request.POST.get(
            "test"), work=request.POST.get("work"))

        if request.POST.get("symptomDemam") != None:
            obj.is_Fever = True
            obj.SympTrue.append("Demam")
        if request.POST.get("symptomSulitBernapas") != None:
            obj.is_HardBreathing = True
            obj.SympTrue.append("Kesulitan Bernapas")
        if request.POST.get("symptomBatuk") != None:
            obj.is_Cough = True
            obj.SympTrue.append("Batuk")
        if request.POST.get("symptomMuntah") != None:
            obj.is_Vommitting = True
            obj.SympTrue.append("Muntah atau diare")
        if request.POST.get("symptomNyeri") != None:
            obj.is_Pain = True
            obj.SympTrue.append("Nyeri")
        if request.POST.get("symptomNull") != None:
            obj.is_No = True
            obj.SympTrue.append("Tidak ada")

        if request.POST.get("Obesity") != None:
            obj.is_Obesity = True
            obj.CondTrue.append("Obesitas")
        if request.POST.get("SmokeVape") != None:
            obj.is_SmokeVape = True
            obj.CondTrue.append("Merokok atau vaping")
        if request.POST.get("Pregnant") != None:
            obj.is_Pregnant = True
            obj.CondTrue.append("Hamil")
        if request.POST.get("CirculatoryDisease") != None:
            obj.is_CirculatoryDisease = True
            obj.CondTrue.append("CirculatoryDisease")
        if request.POST.get("BloodCellDisorder") != None:
            obj.is_BloodCellDisorder = True
            obj.CondTrue.append("BloodCellDisorder")
        if request.POST.get("CondNull") != None:
            obj.is_CondNull = True
            obj.CondTrue.append("Tidak ada")
        obj.save()
        return HttpResponseRedirect('../../Screening/Result/')
    return render(request, 'questionnaire.html')


def result(request):
    context = Answer.objects.latest('id')
    sympList = context.SympTrue
    condList = context.CondTrue
    return render(request, 'result.html', {'context': context, 'sympList': sympList, 'condList': condList, })
